package com.avdim.controller;

import com.avdim.services.AdminService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тесты для тестирования AdminController
 */
@WebMvcTest(AdminController.class)
@TestPropertySource(locations = "/application.properties")
@AutoConfigureMockMvc
class AdminControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Mock
    HttpServletRequest mockHttpRequest;

    @MockBean
    private AdminService mockAdminService;

    /**
     * Тестирование метода при наличии cookie {@link AdminController#adminPanel(HttpServletRequest)}
     */
    @Test
    void testAdminPanelOk() throws Exception {
        Cookie[] cookies = mockHttpRequest.getCookies();
        when(mockAdminService.checkAdminCookie(cookies)).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/admin")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Тестирование метода при отсутствии cookie {@link AdminController#adminPanel(HttpServletRequest)}
     */
    @Test
    void testAdminPanelForbidden() throws Exception {
        Cookie[] cookies = mockHttpRequest.getCookies();
        when(mockAdminService.checkAdminCookie(cookies)).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/admin")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    /**
     * Тестирование метода блокировки пользователя при наличии cookie {@link AdminController#blockedUser(HttpServletRequest, String, String)}
     */
    @Test
    void testBlockedUserAccess() throws Exception {
        Cookie[] cookies = mockHttpRequest.getCookies();
        when(mockAdminService.checkAdminCookie(cookies)).thenReturn(true);
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/admin/blocked")
                        .param("name", "denis")
                        .param("action", "unblock")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Тестирование метода блокировки пользователя при отсутствии cookie {@link AdminController#blockedUser(HttpServletRequest, String, String)}
     */
    @Test
    void testBlockedUserForbidden() throws Exception {
        Cookie[] cookies = mockHttpRequest.getCookies();
        when(mockAdminService.checkAdminCookie(cookies)).thenReturn(false);
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/admin/blocked")
                        .param("name", "denis")
                        .param("action", "unblock")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}