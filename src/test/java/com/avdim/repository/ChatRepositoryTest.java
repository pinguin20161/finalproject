package com.avdim.repository;

import com.avdim.model.Account;
import com.avdim.model.ChatMessage;
import com.avdim.repository.AdminRepository;
import com.avdim.repository.ChatRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Тесты для тестирования {@link ChatRepository}
 */
@SpringBootTest
public class ChatRepositoryTest {

    private Account testAccount = new Account("testName", "testNick", "testPassword");
    private String urlToDatabase;

    @Value("${property.url.database}")
    public void setUrlToDatabase(String urlToDatabase) {
        this.urlToDatabase = urlToDatabase;
    }

    @Autowired
    ChatRepository chatRepository;
    @Autowired
    AdminRepository adminRepository;

    /**
     * Тестирование метода {@link ChatRepository#createAccount(Account)}
     */
    @Test
    public void createAccount_Test() {
        final String sql = "select * from accounts where user_name = 'testName'";
        final String sqlDelete = "delete from accounts where user_name = 'testName'";
        int result = chatRepository.createAccount(testAccount);
        Assertions.assertTrue(1 == result);

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(sql);
            String resultNickName = resultSet.getString("nick_name");
            Assertions.assertTrue("testNick".equals(resultNickName));

            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тестирование метода {@link ChatRepository#findAccount(Account, boolean)}
     */
    @Test
    public void findAccount_Test() {
        final String sqlDelete = "delete from accounts where user_name = 'testName'";

        chatRepository.createAccount(testAccount);
        boolean result = chatRepository.findAccount(testAccount, true);
        Assertions.assertTrue(result);

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тестирование метода {@link ChatRepository#findBlockedUsers(String)}
     */
    @Test
    public void findBlockedUsers_Test() {
        final String sqlDelete = "delete from blocked_users where user = 'TestBlock'";
        adminRepository.actionBlockUnblock("TestBlock", "blocked");

        boolean resultTrue = chatRepository.findBlockedUsers("TestBlock");
        boolean resultFalse = chatRepository.findBlockedUsers("TestNonBlock");

        Assertions.assertTrue(resultTrue);
        Assertions.assertFalse(resultFalse);

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тестирование метода {@link ChatRepository#createHistoryMessage(List)}
     */
    @Test
    public void createHistoryMessage_Test() {
        final String sqlDelete = "delete from messages_history where room = 'testRoom'";
        List<ChatMessage> testList = new ArrayList<>();
        ChatMessage testMessage = new ChatMessage();
        testMessage.setType(ChatMessage.MessageType.CHAT);
        testMessage.setSender("testSender");
        testMessage.setContent("testContent");
        testMessage.setRoom("testRoom");
        testMessage.setTimestamp("testTimestamp");
        testList.add(testMessage);

        Boolean result  = chatRepository.createHistoryMessage(testList);
        Assertions.assertTrue(result);

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тестирование метода {@link ChatRepository#getHistoryMessage(String)}
     */
    @Test
    public void getHistoryMessage_Test() {
        final String sqlDelete = "delete from messages_history where room = 'testRoom'";
        List<ChatMessage> testList = new ArrayList<>();
        ChatMessage testMessage = new ChatMessage();
        testMessage.setType(ChatMessage.MessageType.CHAT);
        testMessage.setSender("testSender");
        testMessage.setContent("testContent");
        testMessage.setRoom("testRoom");
        testMessage.setTimestamp("testTimestamp");
        testList.add(testMessage);
        Boolean result  = chatRepository.createHistoryMessage(testList);

        List<ChatMessage> testListGet = chatRepository.getHistoryMessage("testRoom");
        ChatMessage testMessageGet = testListGet.get(testList.size()-1);

        Assertions.assertTrue("testSender".equals(testMessage.getSender()));

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
