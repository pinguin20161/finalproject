package com.avdim.services;

import javax.servlet.http.Cookie;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.avdim.repository.AdminRepository;
import org.mockito.Mockito;

/**
 * Класс проверки {@link AdminService}
 */
public class AdminServiceTest {

    /**
     * Проверка метода {@link AdminService#getCookieAdmin()}.
     * Проверяется, что cookie имеет ожидаемое имя.
     */
    @Test
    public void getCookieAdminTest() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        AdminService adminService = new AdminService(mockAdminRepository);
        Cookie adminCookie = adminService.getCookieAdmin();

        Assertions.assertEquals("id_admin", adminCookie.getName());
    }

    /**
     * Проверка метода {@link AdminService#checkAdminCookie(Cookie[])}.
     * Проверяется обработка на null входного массива
     */
    @Test
    public void checkAdminCookie_InputNull_Test() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        AdminService adminService = new AdminService(mockAdminRepository);

        Assertions.assertFalse(adminService.checkAdminCookie(null));
    }

    /**
     * Проверка метода {@link AdminService#checkAdminCookie(Cookie[])}.
     * Проверяется обработка на null значений входного массива
     */
    @Test
    public void checkAdminCookie_InputNullElements_Test() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        AdminService adminService = new AdminService(mockAdminRepository);

        Assertions.assertFalse(adminService.checkAdminCookie(new Cookie[5]));
    }

    /**
     * Проверка метода {@link AdminService#checkAdminCookie(Cookie[])}.
     * Проверяется негативный ответ при отсутствии нужного cookie
     */
    @Test
    public void checkAdminCookie_Negative_Test() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        AdminService adminService = new AdminService(mockAdminRepository);
        Cookie[] cookies = new Cookie[]{
                new Cookie("name1", "cookie1"),
                new Cookie("name2", "cookie2"),
                new Cookie("name3", "cookie3")
        };

        Assertions.assertFalse(adminService.checkAdminCookie(cookies));
    }

    /**
     * Проверка метода {@link AdminService#checkAdminCookie(Cookie[])}.
     * Проверяется положительный ответ при наличии нужного cookie
     */
    @Test
    public void checkAdminCookie_Positive_Test() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        AdminService adminService = new AdminService(mockAdminRepository);
        Cookie adminCookie = adminService.getCookieAdmin();
        Cookie[] cookies = new Cookie[]{
                new Cookie("name1", "cookie1"),
                new Cookie("name2", "cookie2"),
                new Cookie("name3", "cookie3"),
                adminCookie
        };

        Assertions.assertTrue(adminService.checkAdminCookie(cookies));
    }

    /**
     * Проверка метода {@link AdminService#actionBlocked(String, String)}.
     * Для проверки используется Mock-объект {@link AdminRepository}.
     * Проверяется формирование негативного ответа при otherName.
     * Проверяется формирование положительного ответа при name.
     */
    @Test
    public void actionBlockedTest() {
        AdminRepository mockAdminRepository = Mockito.mock(AdminRepository.class);
        final String name = "name1";
        final String otherName = "name548";
        final String action = "blocked";
        final int forPositiveResult = 1;
        final int forNegativeResult = 0;
        Mockito.when(mockAdminRepository.actionBlockUnblock(name, action)).thenReturn(forPositiveResult);
        Mockito.when(mockAdminRepository.actionBlockUnblock(otherName, action)).thenReturn(forNegativeResult);
        String positiveResult = "{\"ACT\":\"YES\"}";
        String negativeResult = "{\"ACT\":\"NO\"}";

        AdminService adminService = new AdminService(mockAdminRepository);

        Assertions.assertEquals(negativeResult, adminService.actionBlocked(otherName, action));

        Assertions.assertEquals(positiveResult, adminService.actionBlocked(name, action));
    }
}
