package com.avdim.services;

import com.avdim.model.Account;
import com.avdim.model.ChatMessage;
import com.avdim.repository.ChatRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Класс проверки {@link ChatService}
 */
public class ChatServiceTest {


    /**
     * Проверка метода {@link ChatService#createAccount(Account)}.
     * Сценарий проверки: отказ в создании данного аккаунта, в связи с тем, что такой логин уже зарегистрирован.
     */
    @Test
    public void createAccount_AlwaysExistsFault_Test() {
        final boolean accountFound = true;
        Account mockAccount = Mockito.mock(Account.class);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findAccount(mockAccount, false)).thenReturn(accountFound);

        ChatService chatService =  new ChatService(mockChatRepository);

        Assertions.assertFalse(chatService.createAccount(mockAccount));
    }


    /**
     * Проверка метода {@link ChatService#createAccount(Account)}.
     * Сценарий проверки: отказ в создании данного аккаунта, в связи с возникновением ошибки в репозитории.
     */
    @Test
    public void createAccount_ErrorCreateAccount_Test() {
        final boolean accountFound = false;
        final int resultCreateAccount = 0;
        Account mockAccount = Mockito.mock(Account.class);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findAccount(mockAccount, false)).thenReturn(accountFound);
        Mockito.when(mockChatRepository.createAccount(mockAccount)).thenReturn(resultCreateAccount);

        ChatService chatService =  new ChatService(mockChatRepository);

        Assertions.assertFalse(chatService.createAccount(mockAccount));
    }


    /**
     * Проверка метода {@link ChatService#createAccount(Account)}.
     * Сценарий проверки: аккаунт успешно создан и зарегистрирован в репозитории.
     */
    @Test
    public void createAccount_Success_Test() {
        final boolean accountFound = false;
        final int resultCreateAccount = 1;
        Account mockAccount = Mockito.mock(Account.class);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findAccount(mockAccount, false)).thenReturn(accountFound);
        Mockito.when(mockChatRepository.createAccount(mockAccount)).thenReturn(resultCreateAccount);

        ChatService chatService =  new ChatService(mockChatRepository);

        Assertions.assertTrue(chatService.createAccount(mockAccount));
    }


    /**
     * Проверка метода {@link ChatService#loginToChat(Account, String)}.
     * Успешная аутентификация и авторизация администратора.
     */
    @Test
    public void loginToChatTest_AdminLogin_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String roomId = "roomId";
        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(adminLogin);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(adminPassword);

        Assertions.assertEquals("ADMIN", chatService.loginToChat(mockAccount, roomId));
    }

    /**
     * Проверка метода {@link ChatService#loginToChat(Account, String)}.
     * Попытка аутентификации и авторизации заблокированного пользователя.
     */
    @Test
    public void loginToChatTest_Blocked_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = true;

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);


        Assertions.assertEquals("BLOCK", chatService.loginToChat(mockAccount, roomId));
    }


    /**
     * Проверка метода {@link ChatService#loginToChat(Account, String)}.
     * Неудачная попытка Аутентификации, репозиторий ответил отказом.
     */
    @Test
    public void loginToChatTest_Failed_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = false;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);


        Assertions.assertEquals("FAIL", chatService.loginToChat(mockAccount, roomId));
    }


    /**
     * Проверка метода {@link ChatService#loginToChat(Account, String)}.
     * Проверка подброса исключения в связи с недопустимым значением идентификатора комнаты.
     */
    @Test
    public void loginToChatTest_IllegalArgumentException_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final boolean blocked = false;
        final boolean loginFound = true;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);


        Assertions.assertThrows(IllegalArgumentException.class, () -> chatService.loginToChat(mockAccount, null));
    }


    /**
     * Проверка метода {@link ChatService#loginToChat(Account, String)}.
     * Успешная аутентификация.
     */
    @Test
    public void loginToChatTest_Ok_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = true;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);


        Assertions.assertEquals("OK", chatService.loginToChat(mockAccount, roomId));
    }


    /**
     * Проверка метода {@link ChatService#logoutUser(String, String)}.
     * Выход пользователя, который не был зарегистрирован.
     */
    @Test
    public void logoutUser_Fault_Test() {
        final String login = "login";
        final String roomId = "roomId";

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);

        ChatService chatService =  new ChatService(mockChatRepository);

        Assertions.assertFalse(chatService.logoutUser(login, roomId));
    }


    /**
     * Проверка метода {@link ChatService#logoutUser(String, String)}.
     * Успешный выход пользователя из чата.
     */
    @Test
    public void logoutUser_Success_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = true;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);

        chatService.loginToChat(mockAccount, roomId);

        Assertions.assertTrue(chatService.logoutUser(login, roomId));
    }


    /**
     * Проверка метода {@link ChatService#onlineUser(String)}.
     * Проверка пользователей в отсутствующей "комнате" чата.
     */
    @Test
    public void onlineUser_NobodyUsers_Test() {
        final String roomId = "roomId";
        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        ChatService chatService =  new ChatService(mockChatRepository);

        TreeSet<String> result = chatService.onlineUser(roomId);

        Assertions.assertTrue(result.isEmpty());
    }


    /**
     * Проверка метода {@link ChatService#onlineUser(String)}.
     * Получение множества пользователей в "комнате" чата.
     */
    @Test
    public void onlineUser_SomeUsers_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String[] logins = {"login1", "login2", "login3"};
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = true;

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Account[] mockAccounts = new Account[logins.length];
        for (int i = 0; i < mockAccounts.length; i++) {
            mockAccounts[i] = Mockito.mock(Account.class);
            Mockito.when(mockAccounts[i].getAccountNickName()).thenReturn(logins[i]);
            Mockito.when(mockAccounts[i].getAccountPassword()).thenReturn(password);

            Mockito.when(mockChatRepository.findBlockedUsers(logins[i])).thenReturn(blocked);
            Mockito.when(mockChatRepository.findAccount(mockAccounts[i], true)).thenReturn(loginFound);
        }

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);

        for (Account account: mockAccounts) {
            chatService.loginToChat(account, roomId);
        }

        TreeSet<String> result = chatService.onlineUser(roomId);
        Assertions.assertEquals(logins.length, result.size());
        for (String login : logins) {
            Assertions.assertTrue(result.contains(login));
        }

    }


    /**
     * Проверка метода {@link ChatService#loadHistory(String)}.
     * Загрузка истории сообщений из репозитория, т.к. кэш пуст.
     * Проверяется возвращаемый лист сообщений.
     */
    @Test
    public void loadHistory_EmptyCash_Test() {
        final String roomId = "roomId";
        final ArrayList<ChatMessage> repHistory = new ArrayList<>();
        ChatMessage mockChatMessage1 = Mockito.mock(ChatMessage.class);
        repHistory.add(mockChatMessage1);
        ChatMessage mockChatMessage2 = Mockito.mock(ChatMessage.class);
        repHistory.add(mockChatMessage2);
        ChatMessage mockChatMessage3 = Mockito.mock(ChatMessage.class);
        repHistory.add(mockChatMessage3);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.getHistoryMessage(roomId)).thenReturn(repHistory);

        ChatService chatService =  new ChatService(mockChatRepository);

        List<ChatMessage> result = chatService.loadHistory(roomId);

        Assertions.assertEquals(repHistory, result);
        Mockito.verify(mockChatRepository, Mockito.times(1)).getHistoryMessage(roomId);
    }


    /**
     * Проверка метода {@link ChatService#loadHistory(String)}.
     * Загрузка истории сообщений из кэша.
     * Проверяется количество сообщений и наличие каждого добавленного сообщения.
     */
    @Test
    public void loadHistory_HistoryFromCash_Test() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = true;
        final int maxCountCash = 5;
        final int maxViewHistory = 5;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);
        chatService.setMaxCountCash(maxCountCash);
        chatService.setMaxViewHistory(maxViewHistory);

        chatService.loginToChat(mockAccount, roomId);
        chatService.loadHistory(roomId);

        ChatMessage mockChatMessage1 = Mockito.mock(ChatMessage.class);
        Mockito.when(mockChatMessage1.getRoom()).thenReturn(roomId);
        chatService.saveMessageToHistory(mockChatMessage1);
        ChatMessage mockChatMessage2 = Mockito.mock(ChatMessage.class);
        Mockito.when(mockChatMessage2.getRoom()).thenReturn(roomId);
        chatService.saveMessageToHistory(mockChatMessage2);
        ChatMessage mockChatMessage3 = Mockito.mock(ChatMessage.class);
        Mockito.when(mockChatMessage3.getRoom()).thenReturn(roomId);
        chatService.saveMessageToHistory(mockChatMessage3);

        List<ChatMessage> result = chatService.loadHistory(roomId);

        Assertions.assertEquals(3, result.size());
        Assertions.assertTrue(result.contains(mockChatMessage1));
        Assertions.assertTrue(result.contains(mockChatMessage2));
        Assertions.assertTrue(result.contains(mockChatMessage3));
    }


    /**
     * Проверка метода {@link ChatService#saveMessageToHistory(ChatMessage)}.
     * Сохранение сообщений в репозиторий.
     * Проверяется вызов соответствующего метода репозитория.
     */
    @Test
    public void saveMessageToHistoryTest() {
        final String adminLogin = "admin";
        final String adminPassword = "password";
        final String login = "login";
        final String password = "password";
        final String roomId = "roomId";
        final boolean blocked = false;
        final boolean loginFound = true;
        final int maxCountCash = 3;

        Account mockAccount = Mockito.mock(Account.class);
        Mockito.when(mockAccount.getAccountNickName()).thenReturn(login);
        Mockito.when(mockAccount.getAccountPassword()).thenReturn(password);

        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        Mockito.when(mockChatRepository.findBlockedUsers(login)).thenReturn(blocked);
        Mockito.when(mockChatRepository.findAccount(mockAccount, true)).thenReturn(loginFound);

        ChatService chatService =  new ChatService(mockChatRepository);
        chatService.setAdminLogin(adminLogin);
        chatService.setAdminPassword(adminPassword);
        chatService.setMaxCountCash(maxCountCash);

        chatService.loginToChat(mockAccount, roomId);
        chatService.loadHistory(roomId);


        for (int i = 0; i <= maxCountCash; i++) {
            ChatMessage mockChatMessage = Mockito.mock(ChatMessage.class);
            Mockito.when(mockChatMessage.getRoom()).thenReturn(roomId);
            chatService.saveMessageToHistory(mockChatMessage);
        }

        Mockito.verify(mockChatRepository, Mockito.atLeast(1)).createHistoryMessage(Mockito.any());
    }

    /**
     * Проверка метода {@link ChatService#destroyService()}.
     * Сохранение сообщений в репозиторий.
     * Проверяется вызов соответствующего метода репозитория.
     */
    @Test
    public void destroyServiceTest() {
        ChatRepository mockChatRepository = Mockito.mock(ChatRepository.class);
        ChatService chatService =  new ChatService(mockChatRepository);

        chatService.destroyService();

        Mockito.verify(mockChatRepository, Mockito.times(1)).createHistoryMessage(Mockito.any());
    }
}
