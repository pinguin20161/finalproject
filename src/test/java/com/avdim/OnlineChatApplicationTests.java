package com.avdim;

import com.avdim.controller.AdminController;
import com.avdim.controller.ChatController;
import com.avdim.repository.AdminRepository;
import com.avdim.repository.ChatRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Тест для тестирования приложения
 */
@SpringBootTest
public class OnlineChatApplicationTests {

	@Autowired
	private AdminController adminController;

	@Autowired
	private ChatController chatController;

	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	private ChatRepository chatRepository;


	/**
	 * Тест для тестирования контекста
	 */
    @Test
    public void contextLoads() {
		assertThat(adminController).isNotNull();
		assertThat(adminRepository).isNotNull();
		assertThat(chatController).isNotNull();
		assertThat(chatRepository).isNotNull();
    }

}
