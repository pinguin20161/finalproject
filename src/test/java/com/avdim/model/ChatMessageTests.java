package com.avdim.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChatMessageTests {

    /**
     * Создается объект типа ChatMessage.MessageType
     */
    ChatMessage.MessageType current = ChatMessage.MessageType.CHAT;

    /**
     * Создается объект типа ChatMessage
     */
    ChatMessage chatMessage = new ChatMessage();

    /**
     * Тест проверяет работу метода {@link ChatMessage#getType()}
     */
    @Test
    public void getType_Test(){
        chatMessage.setType(current);
        ChatMessage.MessageType messageType = chatMessage.getType();
        Assertions.assertEquals(current, messageType);
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#setType(ChatMessage.MessageType)}
     */
    @Test
    public void setType_Test(){
        chatMessage.setType(ChatMessage.MessageType.HISTORY);
        Assertions.assertEquals(ChatMessage.MessageType.HISTORY, chatMessage.getType());
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#getContent()}
     */
    @Test
    public void getContent_Test(){
        chatMessage.setContent("content");
        String result = chatMessage.getContent();
        Assertions.assertEquals("content", result);
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#setContent(String)}
     */
    @Test
    public void setContent_Test(){
        chatMessage.setContent("new content");
        Assertions.assertEquals("new content", chatMessage.getContent());
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#getSender()}
     */
    @Test
    public void getSender_Test(){
        chatMessage.setSender("name");
        String result = chatMessage.getSender();
        Assertions.assertEquals("name", result);
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#setSender(String)}
     */
    @Test
    public void setSender_Test(){
        chatMessage.setSender("name");
        Assertions.assertEquals("name", chatMessage.getSender());
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#getTimestamp()}
     */
    @Test
    public void getTimestamp_Test(){
        chatMessage.setTimestamp("2005-10-30 10:45");
        String result = chatMessage.getTimestamp();
        Assertions.assertEquals("2005-10-30 10:45", result);
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#setTimestamp(String)}
     */
    @Test
    public void setTimestamp_Test(){
        chatMessage.setTimestamp("2005-10-30 10:45");
        Assertions.assertEquals("2005-10-30 10:45", chatMessage.getTimestamp());
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#getRoom()}
     */
    @Test
    public void getRoom_Test(){
        chatMessage.setRoom("room");
        String result = chatMessage.getRoom();
        Assertions.assertEquals("room", result);
    }

    /**
     * Тест проверяет работу метода {@link ChatMessage#setRoom(String)}
     */
    @Test
    public void setRoom_Test(){
        chatMessage.setRoom("room");
        Assertions.assertEquals("room", chatMessage.getRoom());
    }

}
