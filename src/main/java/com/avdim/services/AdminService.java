package com.avdim.services;

import com.avdim.repository.AdminRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Класс AdminService обеспечивает взаимодействие {@link com.avdim.controller.AdminController}
 * и {@link com.avdim.repository.AdminRepository} (в разработке).
 */
@Service
public class AdminService {

    /**
     * Множество для хранения cookies.
     */
    private final Set<String> cookiesUUid = new HashSet<>();

    /**
     * Взаимодействие с репозиторием, используется внедрение через конструктор.
     */
    private final AdminRepository adminRepository;

    /**
     * Конструктор с внедрением зависимости {@link com.avdim.repository.AdminRepository}.
     * 
     * @param adminRepository - для внедрения в локальное поле
     */
    public AdminService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    /**
     * Формирует javax.servlet.http.Cookie для проверки прав администратора.
     * Формируется уникальное значение идентификатора, которое хранится в локальном множестве.
     * Создаётся Cookie с допустимым максимальным "возрастом" 5 минут (фиксированное время).
     *
     * @return Cookie с кодом подтверждения прав администратора.
     */
    public Cookie getCookieAdmin() {
        String uuid = generateUUid();
        if (cookiesUUid.isEmpty()) {
            cookiesUUid.add(uuid);
        } else {
            cookiesUUid.clear();
            cookiesUUid.add(uuid);
        }
        Cookie cookie = new Cookie("id_admin", uuid);
        cookie.setMaxAge(5 * 60);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        cookie.setPath("/admin");
        return cookie;
    }

    /**
     * Проверка прав администратора.
     * Анализируется cookies на наличие ключа подтверждения прав администратора.
     * Производится перебор всех cookies на наличие имени "id_admin".
     * При наличии в cookies cookie с заданным именем производится сравнивание его значения (ключа)
     * со сформированными и ранее выданными ключами администратора, которые хранятся
     * в локальном множестве cookiesUUid.
     *
     * @param cookies - массив с Cookies
     * @return true - если в cookies содержится ранее сформированный и выданный ключ администратора;
     *         false - в остальных случаях.
     */
    public boolean checkAdminCookie(Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c != null && "id_admin".equals(c.getName())) {
                    return cookiesUUid.contains(c.getValue());
                }
            }
        }
        return false;
    }

    /**
     * Формирует html-страницу администратора.
     *
     * @return Строка со страницей администратора.
     */
    public String getAdminPage() {

        return "<!DOCTYPE html>" +
                "<html lang=\"en\">" +
                "<head>" +
                "<title>Admin Panel</title>" +
                "<link rel=\"stylesheet\" href=\"/css/main.css\">" +
                "<script src=\"/js/admin.js\"></script>" +
                "</head>" +
                "<body>" +
                "<div id=\"admin-page\">" +
                "<div class=\"username-page-container\">" +
                "<h1 class=\"title\">Управление блокировкой</h1>" +
                "<div class=\"form-group\">" +
                "<input type=\"text\" id=\"name\" placeholder=\"Ник в чате\" autocomplete=\"off\" " +
                        "class=\"form-control\" />" +
                "</div>" +
                "<div class=\"form-group\">" +
                "<button type=\"submit\" onclick=\"unblock()\" style=\"margin-right:15px\" " +
                        "class=\"accent username-submit\">Снять</button>" +
                "<button type=\"submit\" onclick=\"block()\" style=\"margin-right:15px\" " +
                        "class=\"accent username-submit\">Установить</button>" +
                "<button type=\"submit\" onclick=\"document.location='/'\" style=\"margin-right:15px\" " +
                        "class=\"accent username-submit\">Выход</button>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div id=\"admin-message\" class=\"hidden\">" +
                "<div class=\"username-page-container\">" +
                "<h1 id=\"message-view\" class=\"title\"></h1>" +
                "<div>" +
                "<div class=\"form-group\">" +
                "<button type=\"submit\" onclick=\"document.location='/admin'\" style=\"margin-right:15px\" " +
                        "class=\"accent username-submit\">OK</button>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</body>" +
                "</html>";
    }

    /**
     * Формирует строку ответа на команды блокировки/разблокировки пользователя.
     * Команда транслируется методу репозитория {@link AdminRepository#actionBlockUnblock}.
     * Проверяется ответ метода (который, в свою очередь, должен быть количеством изменённых строк)
     * для формирования строки ответа.
     *
     * @param name - имя пользователя для блокировки/разблокировки
     * @param action - определяющее действие строка.
     * @return строка ответа: {"ACT":"NO"} в негативном случае; {"ACT":"YES"} в случае успеха операции.
     */
    public String actionBlocked(String name, String action) {
        int resultAction = adminRepository.actionBlockUnblock(name, action);
        if (resultAction > 0) {
            return "{\"ACT\":\"YES\"}";
        }
        return "{\"ACT\":\"NO\"}";
    }

    /**
     * Генерирует строковое представление универсального уникального идентификатора.
     * Используется стандартная реализация из {@link java.util.UUID}.
     *
     * @return строковое представление универсального уникального идентификатора
     */
    private String generateUUid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
