package com.avdim.services;

import com.avdim.model.Account;
import com.avdim.model.ChatMessage;
import com.avdim.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.*;

@Service
public class ChatService {

    /**
     * Перечень пользователей по "комнатам" чата.
     */
    private final TreeMap<String, Set<String>> usersToRoom = new TreeMap<>();

    /**
     * Кэш сообщений с разбивкой по "комнатам" чата.
     */
    private final TreeMap<String, List<ChatMessage>> historyToRoom = new TreeMap<>();

    /**
     * Перечень пользователей со статусом "онлайн".
     */
    private final TreeSet<String> usersOnline = new TreeSet<>();

    /**
     * Общий кэш сообщений.
     */
    private final List<ChatMessage> historyChatMessages = new ArrayList<>();

    /**
     * Поле для хранения ссылки на репозиторий. Внедрение производится через конструктор.
     */
    private final ChatRepository chatRepository;

    /**
     * Количество сообщений в "комнате" чата, хранящихся в локальном кэше.
     * Производится внедрение из файла свойств.
     */
    private int maxViewHistory;

    /**
     * Предел сообщений в локальном кэше. При превышении предела сообщения сохраняются в репозиторий.
     * Внедрение производится из файла свойств.
     */
    private int maxCountCash;

    /**
     * Поле хранения логина администратора
     */
    private String adminLogin;

    /**
     * Поле хранения пароля администратора
     */
    private String adminPassword;

    /**
     * Конструктор с внедрением зависимости от репозитория.
     *
     * @param chatRepository - ссылка на репозиторий
     */
    public ChatService(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }


    /**
     * Метод установки предела сообщений, хранящихся в локальном кэше "комнаты" чата.
     *
     * @param maxViewHistory - предел количества сообщений, хранящихся в локальном кэше в "комнате" чата.
     */
    @Value("${property.max.view.history}")
    public void setMaxViewHistory(int maxViewHistory) {
        this.maxViewHistory = maxViewHistory;
    }

    /**
     * Метод установки логина администратора.
     * Установка логина администратора производится из файла свойств.
     *
     * @param adminLogin - логин администратора
     */
    @Value("${admin.login}")
    public void setAdminLogin(String adminLogin) {
        this.adminLogin = adminLogin;
    }


    /**
     * Метод установки пароля администратора.
     * Установка пароля администратора производится из файла свойств.
     *
     * @param adminPassword - пароль администратора.
     */
    @Value("${admin.password}")
    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }


    /**
     * Установка предела сообщений в локальном кэше.
     * При превышении предела сообщений, сообщения из локального кэша сохраняются в репозиторий.
     *
     * @param maxCountCash - предел сообщений в локальном кэше.
     */
    @Value("${property.count.cash}")
    public void setMaxCountCash(int maxCountCash) {
        this.maxCountCash = maxCountCash;
    }


    /**
     * Метод создания нового аккаунта.
     * Производится проверка существования уже зарегистрированного аккаунта в репозитории.
     * Если аккаунт уже имеется, то в создании аккаунта отказывается.
     *
     * @param accountArg - аккаунт.
     * @return true - в случае успешного создания нового аккаунта; false - в противном случае.
     */
    public boolean createAccount(Account accountArg) {
        boolean resultFind = chatRepository.findAccount(accountArg, false);
        if (!resultFind) {
            int result = chatRepository.createAccount(accountArg);
            return result == 1;
        }
        return false;
    }


    /**
     * Метод обработки подключения пользователя к чату.
     * Производится аутентификация и авторизация пользователя.
     * Проверяется, является ли пользователь администратором.
     * Проверяется, заблокирован ли пользователь (проверка по репозиторию).
     * Проверяется, зарегистрирован ли пользователь (проверка по репозиторию).
     * Проверяется, двойная авторизация по одному аккаунту.
     *
     * @param accountArg - данные аккаунта пользователя.
     * @param roomId - идентификатор комнаты.
     * @return статус подключения пользователя.
     */
    public String loginToChat(Account accountArg, String roomId) {
        if (roomId == null) {
            throw new IllegalArgumentException(".loginToChat() roomId cannot be null");
        }
        String loginName = accountArg.getAccountNickName();
        String loginPassword = accountArg.getAccountPassword();
        if (loginName.equals(adminLogin) && loginPassword.equals(adminPassword)) {
            return "ADMIN";
        }
        if (chatRepository.findBlockedUsers(accountArg.getAccountNickName())) {
            return "BLOCK";
        }

        boolean resultRequest = chatRepository.findAccount(accountArg, true);
        if (resultRequest) {
            boolean isAdded = usersOnline.add(loginName);
            if (isAdded) {
                addUserToRoom(loginName, roomId);
                return "OK";
            }
            return "CLASH";
        }
        return "FAIL";
    }


    /**
     * Добавление пользователя в "комнату" чата.
     * Если на момент добавления пользователя "комната" чата отсутствует, то она создаётся,
     * как в кэше сообщений по "комнатам" чата, так и в перечне пользователей по "комнатам" чата.
     *
     * @param loginName - логин пользователя.
     * @param roomId - идентификатор комнаты чата.
     */
    private void addUserToRoom(String loginName, String roomId) {
        if (usersToRoom.containsKey(roomId)) {
            usersToRoom.get(roomId).add(loginName);
        } else {
            TreeSet<String> treSetUsers = new TreeSet<>();
            treSetUsers.add(loginName);
            usersToRoom.put(roomId, treSetUsers);
        }
    }


    /**
     * Обработка выхода пользователя из "комнаты" чата.
     * Производится исключения пользователя из перечня пользователей по "комнатам" чата
     * и из перечня пользователей со статусом "онлайн".
     *
     * @param loginName - логин пользователя
     * @param roomId - идентификатор "комнаты" чата
     * @return true - если пользователь был зарегистрирован в перечне онлайн-пользователей.
     */
    public boolean logoutUser(String loginName, String roomId) {
        if (usersToRoom.containsKey(roomId)) {
            usersToRoom.get(roomId).remove(loginName);
        }
        return usersOnline.remove(loginName);
    }


    /**
     * Метод возвращает множество, содержащее перечень пользователей в заданной "комнате" чата.
     *
     * @param roomId - идентификатор комнаты.
     * @return Множество пользователей в заданной "комнате" чата.
     */
    public TreeSet<String> onlineUser(String roomId) {
        if (usersToRoom.containsKey(roomId)) {
            return (TreeSet<String>) usersToRoom.get(roomId);
        }
        return new TreeSet<>();
    }


    /**
     * Метод возвращает заданное количество последних сообщений "комнаты" чата.
     * История сообщений загружается из локального кэша. В случае,
     * если в локальном кэше отсутствует идентификатор "комнаты" чата,
     * то история сообщений подгружается из репозитория с сохранением в локальный кэш.
     *
     * @param roomId - идентификатор "комнаты" чата.
     * @return Лист с заданным количеством последних сообщений "комнаты" чата.
     */
    public List<ChatMessage> loadHistory(String roomId) {
        List<ChatMessage> messageHistoryRoom;
        if (historyToRoom.containsKey(roomId)) {
            messageHistoryRoom = historyToRoom.get(roomId);
        } else {
            messageHistoryRoom = chatRepository.getHistoryMessage(roomId);
            historyToRoom.put(roomId, messageHistoryRoom);
        }
        return messageHistoryRoom;
    }


    /**
     * Запись сообщения в локальный кэш.
     * При добавлении сообщения производится проверка лимита сообщений в "комнате" чата.
     * (Лимит сообщений устанавливается в файле свойств).
     * При превышении лимита сообщений в "комнате" чата старые сообщения удаляются из локального кэша.
     * Сообщения также хранятся в общем кэше сообщений, при превышении предела сообщений в общем кэше сообщений
     * они сохраняются в репозитории.
     *
     * @param message - сообщение.
     */
    public void saveMessageToHistory(ChatMessage message) {

        String roomId = message.getRoom();
        historyToRoom.get(roomId).add(message);
        if (historyToRoom.get(roomId).size() > maxViewHistory) {
            List<ChatMessage> someLastMessages = new ArrayList<>();
            for (int i = historyToRoom.get(roomId).size() - maxViewHistory; i < historyToRoom.get(roomId).size(); i++) {
                someLastMessages.add(historyToRoom.get(roomId).get(i));
            }
            historyToRoom.remove(roomId);
            historyToRoom.put(roomId, someLastMessages);
        }

        historyChatMessages.add(message);
        if (historyChatMessages.size() > maxCountCash - 1) {
            if (chatRepository.createHistoryMessage(historyChatMessages)) {
                historyChatMessages.clear();
            }
        }
    }


    /**
     * Запись кэша сообщений в репозиторий при завершении.
     */
    @PreDestroy
    public void destroyService() {
        chatRepository.createHistoryMessage(historyChatMessages);
    }
}
