package com.avdim.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.sql.*;
import java.util.Date;

/**
 * Класс AdminRepository - содержит метод для блокировки и разблокировки аккаунта
 *
 * @author Ермаков Анатолий
 * @version 1.0
 */
@Repository
public class AdminRepository {

    /**
     * Приватное поле URL внедряемого через setter injection
     */
    private String urlToDatabase;

    /**
     * Внедрение URL из файла application.properties
     */
    @Value("${property.url.database}")
    public void setUrlToDatabase(String urlToDatabase) {
        this.urlToDatabase = urlToDatabase;
    }

    /**
     * Метод необходим для блокировки и разблокировки аккаунта
     *
     * @param name - NickName аккаунта
     * @param action - действие
     * @return - int
     */
    public int actionBlockUnblock(String name, String action) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        if (action.equals("blocked")) {
            if (findBlockedUsers(name)) return 0;
        } else if (action.equals("unblocked")) {
            if (!findBlockedUsers(name)) return 0;
        }
        String createPrepareRequest;
        if (action.equals("blocked")) {
            createPrepareRequest = "insert into blocked_users (user, lock_date ) values (?,?);";
        } else if (action.equals("unblocked")) {
            createPrepareRequest = "delete from blocked_users where user = ?;";
        } else return 0;
        int row;
        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            if (action.equals("blocked")) {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, new Date().toString());
            } else {
                preparedStatement.setString(1, name);
            }
            row = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error record: " + e.getMessage());
            e.printStackTrace();
            return 0;
        }
        return row;
    }

    /**
     * Метод проверяет заблокирован аккаунт или нет, необходим для работы метода actionBlockUnblock
     *
     * @param name - NickName аккаунта
     * @return - boolean
     */
    private boolean findBlockedUsers(String name) {
        String createPrepareRequest = "select user from blocked_users where user = ?;";
        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println("Error record: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
