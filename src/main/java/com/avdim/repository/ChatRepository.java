package com.avdim.repository;

import com.avdim.model.Account;
import com.avdim.model.ChatMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс ChatRepository - содержит методы:
 * - для записи и извлечения истории сообщений из БД
 * - для записи и проверки наличия аккаунта в БД
 * - для проверки заблокированного аккаунта в БД
 *
 * @author Ермаков Анатолий
 * @version 1.0
 */
@Repository
public class ChatRepository {
    /**
     * Приватное поле URL внедряемого через setter injection
     */
    private String urlToDatabase;

    /**
     * Приватное поле maxViewHistory внедряемого через setter injection
     */
    private int maxViewHistory;

    /**
     * Приватное поле adminLogin внедряемого через setter injection
     */
    private String adminLogin;

    /**
     * Внедрение URL из файла application.properties
     */
    @Value("${property.url.database}")
    public void setUrlToDatabase(String urlToDatabase) {
        this.urlToDatabase = urlToDatabase;
    }

    /**
     * Внедрение логина админа из файла application.properties
     */
    @Value("${admin.login}")
    public void setAdminLogin(String adminLogin) {
        this.adminLogin = adminLogin;
    }

    /**
     * Внедрение максимального количества сообщений загружаемых при входе в чат
     * из файла application.properties
     */
    @Value("${property.max.view.history}")
    public void setMaxViewHistory(int maxViewHistory) {
        this.maxViewHistory = maxViewHistory;
    }

    /**
     * Метод создает новый аккаунт
     *
     * @param account - содержит данные аккаунта
     * @return - int
     */
    public int createAccount(Account account) {
        if (account == null) {
            throw new NullPointerException();
        }

        if (account.getAccountNickName().toLowerCase().equals(adminLogin)) {
            return 0;
        }

        final String createPrepareRequest = "insert into accounts (user_name, nick_name, password ) values (?,?,?);";
        int row;
        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            preparedStatement.setString(1, account.getAccountName());
            preparedStatement.setString(2, account.getAccountNickName());
            preparedStatement.setString(3, account.getAccountPassword());
            row = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error create account: " + e.getMessage());
            e.printStackTrace();
            return 0;
        }
        return row;
    }

    /**
     * Метод ищет аккаунт в БД
     *
     * @param account - содержит данные аккаунта
     * @param isLogin - boolean
     * @return - boolean
     */
    public boolean findAccount(Account account, boolean isLogin) {
        if (account == null) {
            throw new IllegalArgumentException();
        }

        String createPrepareRequest;
        if (isLogin) {
            createPrepareRequest = "select * from accounts where nick_name = ? and password = ?;";
        } else {
            createPrepareRequest = "select * from accounts where nick_name = ?;";
        }

        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            if (isLogin) {
                preparedStatement.setString(1, account.getAccountNickName());
                preparedStatement.setString(2, account.getAccountPassword());
            } else {
                preparedStatement.setString(1, account.getAccountNickName());
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            System.out.println("Error find account: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Метод проверяет заблокирован аккаунт или нет
     *
     * @param name - NickName аккаунта
     * @return - boolean
     */
    public boolean findBlockedUsers(String name) {
        final String createPrepareRequest = "select user from blocked_users where user = ?;";
        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println("Error record: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Метод записывает сообщения в БД
     *
     * @param listFullHistory - коллекция сообщений из кэша
     * @return - boolean
     */
    public boolean createHistoryMessage(List<ChatMessage> listFullHistory) {
        final String createPrepareRequest = "insert into messages_history (sender, content, room, timestamp) values (?,?,?, ?);";
        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            for (ChatMessage chatMessage : listFullHistory) {
                preparedStatement.setString(1, chatMessage.getSender());
                preparedStatement.setString(2, chatMessage.getContent());
                preparedStatement.setString(3, chatMessage.getRoom());
                preparedStatement.setString(4, chatMessage.getTimestamp());
                preparedStatement.executeUpdate();
            }
            return true;
        } catch (Exception ex) {
            System.out.println("Error create historyMessage: " + ex.getMessage());
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * Метод возвращает 5 последних сообщений из БД, количество сообщений можно настраивать
     *
     * @param roomId - номер комнаты чата
     * @return - коллекцию сообщений
     */
    public List<ChatMessage> getHistoryMessage(String roomId) {
        List<ChatMessage> list = new ArrayList<>();
        final String createPrepareRequest = "select id, sender, content, room, timestamp from messages_history where room = ? order by id desc limit ?;";

        try (Connection connection = DriverManager.getConnection(urlToDatabase);
             PreparedStatement preparedStatement = connection.prepareStatement(createPrepareRequest)) {
            preparedStatement.setString(1, roomId);
            preparedStatement.setInt(2, maxViewHistory);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String sender = resultSet.getString("sender");
                String content = resultSet.getString("content");
                String room = resultSet.getString("room");
                String timestamp = resultSet.getString("timestamp");
                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setSender(sender);
                chatMessage.setContent(content);
                chatMessage.setRoom(room);
                chatMessage.setTimestamp(timestamp);
                chatMessage.setType(ChatMessage.MessageType.HISTORY);
                list.add(chatMessage);
            }
            List<ChatMessage> listTemp = new ArrayList<>();
            if (list.size() > 1) {
                for (int i = list.size() - 1; i >= 0; i--) {
                    listTemp.add(list.get(i));
                }
                list.clear();
                list.addAll(listTemp);
            }
        } catch (SQLException ex) {
            System.out.println("Error get historyMessage: " + ex.getMessage());
            ex.printStackTrace();
        }
        return list;
    }

}
