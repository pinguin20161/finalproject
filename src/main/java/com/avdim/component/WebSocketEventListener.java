package com.avdim.component;

import com.avdim.model.ChatMessage;
import com.avdim.services.ChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Objects;

/**
 * Класс WebSocketEventListener создает прослушиватель событий загрузки web страницы приложения,
 * подключения и отключения клиента от сервера WebSocket.
 *
 * @author Медянцев Денис
 * @version 1.0
 */
@Component
public class WebSocketEventListener {

    /**
     * Приватное поле внедряемого экземпляра ChatService
     */
    private final ChatService chatService;

    /**
     * Приватное поле внедряемого экземпляра SimpMessageSendingOperations
     */
    private final SimpMessageSendingOperations messagingTemplate;

    /**
     * Приватное поле внедряемого экземпляра Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    /**
     * Конструктор
     *
     * @param chatService - параметр содержит внедряемый объект сервиса ChatService
     * @param messagingTemplate - параметр содержит шаблон обмена сообщениями SimpMessageSendingOperations
     */
    public WebSocketEventListener(ChatService chatService, SimpMessageSendingOperations messagingTemplate) {
        this.chatService = chatService;
        this.messagingTemplate = messagingTemplate;
    }

    /**
     * Метод вызывается, когда пользователь открывает основную форму приложения в браузере
     * устанавливая соединение с сервером WebSocket.
     *
     * @param event - Событие подключения представляет собой ответ сервера на запрос подключения клиента.
     */
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    /**
     * Метод вызывается, когда пользователь закрывает страницу приложения в браузере
     * разрывая соединение c сервером WebSocket
     *
     * @param event - Событие при закрытии сеанса клиента WebSocket.
     */
    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) Objects.requireNonNull(headerAccessor.getSessionAttributes()).get("username");
        String roomId = (String) Objects.requireNonNull(headerAccessor.getSessionAttributes()).get("room");
        if (username != null) {
            if (chatService.logoutUser(username, roomId)) {
                logger.info("User Disconnected : " + username);
                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setType(ChatMessage.MessageType.LEAVE);
                chatMessage.setSender(username);
                messagingTemplate.convertAndSend("/topic/" + roomId + "/public", chatMessage);
            }
        }
    }
}
